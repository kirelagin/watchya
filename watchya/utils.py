import os

def microseconds(td):
    return td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6

def which(program, wd=None):
    # http://stackoverflow.com/questions/377017/test-if-executable-exists-in-python

    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if wd:
            program = os.path.join(wd, program)
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None
