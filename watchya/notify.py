import os

from twisted.internet import protocol
from twisted.python import log


NOTIFY_SOCKET = '@/ru/kirelagin/watchya/notify'

def socket_path():
    return os.path.join(NOTIFY_SOCKET, str(os.getpid()))

class NotifyProtocol(protocol.DatagramProtocol):
    """Protocol used with sd_notify sockets."""

    def __init__(self, manager):
        self._manager = manager

    def datagramReceived(self, datagram, addr):
        lines = datagram.split('\n')
        data = {}
        for line in lines:
            k, _, v = line.partition('=')
            data[k] = v
        self._manager._process_notification(data)
