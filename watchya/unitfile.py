"""Module provides unit configuration-related routines."""

from collections import defaultdict
import logging
log = logging.getLogger(__name__)
import os.path


class Unit(object):
    """Class representing unit configuration."""

    def __init__(self, unit_dir, name):
        """Load unit-file.

        This function loads unit-file with a given name from a given directory.

        """
        self._name = name
        self._escaped_name = self.escape_name(name)
        self._path = os.path.abspath(os.path.join(unit_dir, self._escaped_name))

        log.info('Trying to load unit from "{0}"...'.format(self._path))
        with open(self._path, 'rt') as f:
            self._sections = self.read_unit_from_file(f)

    def get(self, section, option, default=None):
        """Return value of an option, specified only once.

        If the option was not found in the config file, return default.
        If the option was specified more than once, issue a warning and use the first occurence.

        """
        l = self.get_multiple(section, option, default)
        if len(l) > 1:
            log.warning('Option "{0}" may be specified only once. Using the first one.'.format(option))
            self._sections[section][option] = [l[0]] # warn only once
        return l[0]

    def get_multiple(self, section, option, default=None):
        """Return list of values of an option, specified probably more than once.

        If the option was not found in the config file, return a list, consisting
        of only one default value.

        """
        sect = self._sections[section]
        if not option in sect or len(sect[option]) == 0:
            return [default]
        return sect[option]

    def get_bool(self, section, option, default=None):
        """Returns a boolean value. See also get."""
        val = self.get(section, option)
        if val is None:
            return default
        if val in ('1', 'yes', 'true', 'on'):
            return True
        elif val in ('0', 'no', 'false', 'off'):
            return False
        else:
            raise UnitFileError(section,
                                '"{0}" is not a valid value for boolean option "{1}"'.format(val, option))

    @property
    def path(self):
        return self._path

    @property
    def name(self):
        return self._name

    @property
    def escaped_name(self):
        return self._escaped_name

    @property
    def sections(self):
        return self._sections

    @staticmethod
    def read_unit_from_file(f):
        """Read unit configuration from a file.

        This function reads a systemd-style unit config and returns a dictionary
        of sections. Each section is, in turn, a dictionary of options.
        Each option is a list of strings (because every option may be
        specified more than once).

        """
        sections = defaultdict(lambda : defaultdict(list))

        current_section = ''
        prev_line = None
        for line in f:
            line = line.strip()
            if prev_line:
                line = prev_line + ' ' + line
                prev_line = None

            if line.startswith(('#', ';')) or len(line) == 0:
                pass
            elif line.startswith('['):
                current_section = line[1:].partition(']')[0]
            elif line.endswith('\\'):
                prev_line = line[:-1]
            else:
                opt, sep, val = line.partition('=')
                if not sep:
                    log.warning('Meaningless line: "{0}". Ignoring.'.format(line))
                else:
                    sections[current_section][opt.strip()].append(val.strip())

        return sections

    @staticmethod
    def escape_name(name):
        """systemd-style unit name escaping."""
        new_name = ''
        for c in name:
            if c.isalnum() or c in ('_', '.', '-', ':'):
                new_name += c
            elif c == '/':
                new_name += '-'
            else:
                new_name += '\\x{0:02x}'.format(ord(c))
        return new_name


class UnitFileError(Exception):
    """Raised when unit configuration file contains errors."""
    def __init__(self, section, msg):
        self.section = section
        self.msg = msg

    def __str__(self):
        return 'Section [{0}]: {1}'.format(self.section, self.msg)
