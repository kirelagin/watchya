class FilterBelow(object):
    def __init__(self, lvl):
        self._lvl = lvl

    def filter(self, record):
        return record.levelno < self._lvl
