import errno
import logging
log = logging.getLogger(__name__)
import os
import shlex
import signal

from twisted.python.failure import Failure
from twisted.internet.error import ProcessDone, ProcessTerminated, ProcessExitedAlready
from twisted.internet.defer import Deferred
from twisted.internet.task import LoopingCall

from watchya.logfilter import NamedPipe, LogFilter
from watchya import notify
from watchya import process
from watchya.unitfile import UnitFileError
from watchya.utils import which
from watchya.collector import CpuStatsCollector


class Manager(object):
    """Manages execution of services."""

    def __init__(self, unit):
        """Initizlise manager.

        Takes a Unit object.

        Raises UnitFileError if there are errors in the unit configuration.

        """
        self._unit = unit
        self._socket_path = notify.socket_path()
        self._notify_proto = None
        self._process_proto = None

        self._child = None
        self._process = None
        self._running = False       # True if we are sure the process was started
        self._failure = False       # This is for Restart=on-failure
        self._status = None         # Status as string
        self._shutting_down = False # Will be set to a True 'before' 'shutdown'

        self._terminator = None
        self._watchdog = None       # Watchdog will be created after process started
        self._d_terminate = None    # Deferred created by _terminate_process
        self._check_forked = None   # This will be a LoopingCall checking for presence of the forked process
        self._logpipe = None        # Pipe for filtering logfile

        self._load_config()

    def _load_config(self):
        unit = self._unit

        if '' in unit.sections:
            log.warning('Unit-file has assignments outside of section. Ignoring.')
        if not 'Service' in unit.sections:
            raise UnitFileError('Service', 'Section [Service] is mandatory.')

        def ensure_oneof(section, option, vals, default=None):
            v = unit.get(section, option, default)
            if v is not None and v not in vals:
                raise UnitFileError(section, 'Value "{0}" is not allowed for option "{1}"'.format(v, option))
            return v

        self._type = ensure_oneof('Service', 'Type', ('simple', 'forking', 'notify'), 'simple')

        cmd = unit.get('Service', 'ExecStart')
        if not cmd:
            raise UnitFileError('Service', '"ExecStart" option must be present')
        cmd = shlex.split(cmd)
        self._exe = cmd[0]
        self._args = [os.path.basename(cmd[0])] + cmd[1:]
        self._workdir = os.path.dirname(unit.path)

        pf = unit.get('Service', 'PIDFile')
        if pf and self._type != 'forking':
            log.warning('"PIDFile" option will have no effect for service type "{0}". Ignoring.'.format(s_t))
        if self._type == 'forking' and not pf:
            raise UnitFileError('Service', '"PIDFile" option must be present for service type "forking"')
        self._pidfile = os.path.join(self._workdir, pf) if pf else None

        self._watchdog_seconds = int(unit.get('Service', 'WatchdogSec', 0))

        self._notify_access = ensure_oneof('Service', 'NotifyAccess', ('no', 'all'), 'all')
        if self._type == 'notify' and self._notify_access == 'no':
            raise UnitFileError('Service', '"NotifyAccess" can not be "no" with Type=notify')

        if unit.get('Service', 'KillMode', 'process') != 'process':
            raise UnitFileError('Service', '"KillMode" must be "process"')
        if unit.get_bool('Service', 'SendSIGKILL', True) != True:
            raise UnitFileError('Service', '"SendSIGKILL" can not be disabled')

        def get_sig(name, option):
            if not name.startswith('SIG') or not hasattr(signal, name):
                raise UnitFileError('Service', '"{0}" is not a valid signal in "{1}"'.format(name, option))
            if name.startswith('SIG_'):
                raise UnitFileError('Service', 'No, signals don not have underscores in their names'.format(name))
            return getattr(signal, name)

        self._killsignal = get_sig(unit.get('Service', 'KillSignal', 'SIGTERM'), 'KillSignal')
        self._timeout_stop = int(unit.get('Service', 'TimeoutStopSec', 90))

        def parse_codes_signals_line(line, option):
            codes = set()
            signals = set()
            for v in line.split():
                try:
                    code = int(v)
                    codes.add(code)
                except ValueError:
                    signals.add(get_sig(v, option))
            return (codes, signals)
        s = unit.get('Service', 'SuccessExitStatus', '')
        self._success_exit_codes, self._success_exit_signals = parse_codes_signals_line(s, 'SuccessExitStatus')
        self._success_exit_codes.add(0)
        self._success_exit_signals.update((signal.SIGHUP, signal.SIGINT, signal.SIGTERM, signal.SIGPIPE))
        s = unit.get('Service', 'RestartPreventExitStatus', '')
        self._restart_preventing_exit_codes, self._restart_preventing_exit_signals = parse_codes_signals_line(s, 'RestartPreventExitStatus')

        self._restart_on = ensure_oneof('Service', 'Restart', ('no', 'on-success', 'on-failure', 'on-abort', 'always'), 'no')

        i = unit.get('Service', 'X-ForkedCheckInterval', 10 if self._type == 'forking' else None)
        if i and self._type != 'forking':
            log.warning('"X-ForkedCheckInterval" option will have no effect for service type "{0}". Ignoring.'.format(s_t))
        self._check_forked_interval = int(i) if i else None

        self._stats_interval = int(unit.get('Service', 'X-CpuStatsInterval', 10))

        lf = unit.get('Service', 'X-LogFile', None)
        self._logfile = os.path.join(self._workdir, lf) if lf else None
        self._logmatch = unit.get('Service', 'X-LogMatch', None)

    @property
    def unit(self):
        """Associated Unit object."""
        return self._unit

    @property
    def socket_path(self):
        """Notify-socket path."""
        return self._socket_path

    @property
    def status(self):
        return self._status
    @status.setter
    def status(self, value):
        self._status = value
        log.info('New status: "{0}".'.format(value))

    def run(self, reactor, *args, **kwargs):
        """Set up UNIX socket, spawn process, run reactor.
        
        unitfile.UnitFileError is raised if executable can't be found.

        NB: this method won't return until the end of our execution.
        
        """
        self._reactor = reactor

        def s(manager):
            manager._shutting_down = True
            return manager._terminate_process()

        reactor.addSystemEventTrigger('before', 'shutdown', s, self)

        if self._notify_access != 'no':
            self._notify_proto = notify.NotifyProtocol(self)
            reactor.listenUNIXDatagram('\0' + self.socket_path[1:],
                                       self._notify_proto)

        if self._logfile is not None:
            self._logpipe = NamedPipe(self._logfile, LogFilter(self._logmatch), self._reactor)
            self._logpipe.startReading()

        self._process_proto = process.ManagedProcessProtocol(self)
        self._spawn_process()

        if self._stats_interval:
            collector = CpuStatsCollector(self)
            reactor.callWhenRunning(collector.run, self._reactor, self._stats_interval)

        reactor.run(*args, **kwargs)

    def process_started(self):
        """The managed process is running."""
        self._running = True
        self.status = 'started'
        if self._watchdog_seconds:
            log.info('Setting watchdog to {0} seconds.'.format(self._watchdog_seconds))
            def t(manager):
                manager._watchdog = None
                manager.status = 'Watchdog timed out'
                manager._failure = True
                manager._terminate_process()
            self._watchdog = self._reactor.callLater(self._watchdog_seconds,
                                                     t, self)

    def process_ended(self, status):
        """The managed process exited."""
        self._cancel(self._watchdog)
        self._watchdog = None
        self._cancel(self._terminator)
        if self._check_forked is not None:
            self._check_forked.stop()
            self._check_forked = None
        self._terminator = None
        self._running = False

        exit_code = sig = None
        if status.type == ProcessDone:
            exit_code = 0
        elif status.type == ProcessTerminated:
            exit_code, sig = status.value.exitCode, status.value.signal
        else:
            raise Exception('Something weird happened to the process.')
            # TODO: hmmm...

        if self._pidfile is not None and os.path.isfile(self._pidfile):
            try:
                os.remove(self._pidfile)
            except:
                log.warning('Could not remove pidfile.')

        if exit_code is not None and exit_code in self._success_exit_codes:
            success = True
        elif sig is not None and sig in self._success_exit_signals:
            success = True
        else:
            success = False

        reason = 'exit code {0}'.format(exit_code) if exit_code is not None else 'signal {0}'.format(sig)
        self.status = '{0} ({1})'.format('exited' if success else 'failed', reason)

        d = self._d_terminate
        if d is not None:
            d.callback((exit_code, sig))

        # See Restart= in man systemd.service
        if self._restart_on == 'no':
            need_restart = False
        elif self._restart_on == 'on-success':
            need_restart = success
        elif self._restart_on == 'on-failure':
            need_restart = self._failure or not success
        elif self._restart_on == 'on-abort':
            need_restart = signal is not None
        else:
            need_restart = True
        self._failure = False

        if exit_code is not None and exit_code in self._restart_preventing_exit_codes:
            need_restart = False
        elif sig is not None and sig in self._restart_preventing_exit_signals:
            need_restart = False

        if need_restart and self._shutting_down:
            log.info('System is shutting down. Not restarting.')
            need_restart = False

        if need_restart:
            self.status = 'restarting'
            if self._logpipe:
                self._logpipe.stopReading()
                self._logpipe.startReading()
            self._spawn_process()
        else:
            if not self._shutting_down:
                log.info('Stopping reactor.')
                self._reactor.stop()

    def _spawn_process(self):
        new_env = os.environ.copy()
        if self._notify_proto:
            new_env['NOTIFY_SOCKET'] = self.socket_path
        if self._watchdog_seconds:
            new_env['WATCHDOG_USEC'] = str(self._watchdog_seconds * 10**6)
        self.status = 'starting'
        if not which(self._exe, self._workdir):
            # There is a race condition here.
            # If the executable is removed between this check and actual exec,
            # the forked process will exit with status 1, and we can't tell
            # if wheter it exec'ed or not.
            raise UnitFileError('Service',
                                'Executable specified in "ExecStart" was not found')
        self._child   = self._reactor.spawnProcess(self._process_proto,
                                                   self._exe,
                                                   args=self._args,
                                                   env =new_env,
                                                   path=self._workdir,
                                                  )

    def _terminate_process(self):
        if self._d_terminate is not None:
            return self._d_terminate

        d = Deferred()
        # TODO: run ExecStop first
        if not self._process:
            d.callback(None)
            return d

        def r(_, manager):
            manager._d_terminate = None
        d.addBoth(r, self)
        self._d_terminate = d
        def t(manager):
            manager._terminator = None
            log.info('Sending SIGKILL to the process.')
            try:
                manager._process.signalProcess(signal.SIGKILL)
            except (ProcessExitedAlready, AttributeError):
                pass
            if manager._check_forked is not None:
                # Force check in a second
                manager._check_forked()

        log.info('Sending signal {0} to the process. SIGKILL in {1} seconds.'.format(self._killsignal, self._timeout_stop))
        try:
            self._process.signalProcess(self._killsignal)
        except (ProcessExitedAlready, AttributeError):
            d.callback(None)
            return d

        self._terminator = self._reactor.callLater(self._timeout_stop, t, self)
        return d

    @staticmethod
    def _cancel(delayed):
        if delayed:
            try:
                delayed.cancel()
            except (error.AlreadyCalled, error.AlreadyCancelled):
                pass

    # The following methods might have somewhat misleading names.
    # They are for internal use only, but anyway,
    # don't let them fool you!
    def _process_created(self, process):
        """This method will be called by ManagedProcessProtocol."""
        if self._type == 'simple':
            self._process = process
            self.process_started()

    def _process_ended(self, status):
        """This method will be called by ManagedProcessProtocol."""
        self._child = None
        if self._type == 'forking':
            # Subprocess just forked, so we consider it running now.
            self._read_pid()
            self.process_started()

            def w(manager):
                try:
                    if manager._process is None:
                        return
                    os.kill(manager._process.pid, 0)
                except OSError as e:
                    if e.errno == errno.ESRCH:
                        # Process does not exist
                        manager._process = None
                        manager.process_ended(Failure(ProcessDone(None)))
            self._check_forked = LoopingCall(w, self)
            log.info('Checking for presence of the forked process every {0} seconds.'.format(self._check_forked_interval))
            self._check_forked.start(self._check_forked_interval)
        else:
            self._process = None
            self.process_ended(status)

    def _read_pid(self):
        try:
            with open(self._pidfile, 'rt') as f:
                pid = int(f.read().strip())
            self._process = process.ForkedProcess(pid)
        except (IOError, ValueError) as e:
            log.error('Error reading PID: {0}.'.format(str(e)))
            self._reactor.stop()

    def _process_notification(self, data):
        """This method will be called by NotifyProtocol."""
        if self._type == 'notify':
            if 'READY' in data and data['READY']:
                self._process = self._child
                self.process_started()
        if 'STATUS' in data:
            self.status = data['STATUS']
        if 'ERRNO' in data:
            log.warning('The managed process sent us ERRNO message via sd_notify. ' +
                      + 'This message is not supported, but seems that something bad ' +
                      + 'happened =(.')
        if 'MAINPID' in data:
            log.warning('The managed process sent us MAINPID message via sd_notify. ' +
                      + 'This message is not supported. Ignoring.')
        if 'WATCHDOG' in data and data['WATCHDOG']:
            if self._watchdog:
                self._watchdog.reset(self._watchdog_seconds)
