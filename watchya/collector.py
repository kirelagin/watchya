"""Statistics collector."""

from abc import ABCMeta, abstractmethod
import logging
log = logging.getLogger(__name__)

from twisted.internet.task import LoopingCall


class StatsCollector(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        self._lc = None
        super(StatsCollector, self).__init__()

    def run(self, reactor, interval, now=True):
        """Start this collector."""
        if self._lc is not None:
            return False

        self._interval = interval
        self._on_starting()

        self._lc = LoopingCall(self.collect)
        reactor.addSystemEventTrigger('during', 'shutdown', self.stop)
        self._lc.start(interval, now)
        return True

    def stop(self):
        """Stop this collector."""
        if self._lc is None:
            return False
        self._lc.stop()
        self._lc = None

        self._on_stopped()
        return True

    @abstractmethod
    def collect(self):
        """Do job."""
        pass

    def _on_starting(self):
        """Override me."""
        pass
    def _on_stopped(self):
        """Override me."""
        pass

    @property
    def is_running(self):
        return self._lc is not None


class CpuStatsCollector(StatsCollector):
    def __init__(self, manager, **kwargs):
        self._manager = manager
        self._last_pid = None
        super(CpuStatsCollector, self).__init__(**kwargs)
        self._log = logging.getLogger(__name__ + '.cpu')

    def collect(self):
        if not self._manager._process:
            self._log.debug('Process is not running.')
            return
        try:
            pid = self._manager._process.pid
            if not pid:
                return

            with open('/proc/stat', 'rt') as f:
                total = sum(map(int, f.readline().split()[1:]))
            with open('/proc/{0}/stat'.format(pid), 'rt') as f:
                line = f.readline()
            l,s,line = line.rpartition(')')
            utime, stime = map(int, line.split()[11:13])

            if self._last_pid is None:
                self._log.debug('Initialising.')
                self._first_total_time = total
                self._sum_utime = utime
                self._sum_stime = stime
            elif pid != self._last_pid:
                self._log.debug('PID changed.')
                self._sum_utime += self._last_utime
                self._sum_stime += self._last_stime
            else:
                ufrac, sfrac = self.calc_fracs(total - self._last_total_time,
                                               utime - self._last_utime,
                                               stime - self._last_stime)
                self._log.info('CPU: {0:0.2f} user, {1:0.2f} system.'.format(ufrac, sfrac))

            self._last_pid = pid
            self._last_total_time = total
            self._last_utime = utime
            self._last_stime = stime
        except:
            self._log.warning('Failed to collect CPU stats. Will try next time.')

    @staticmethod
    def calc_fracs(total, utime, stime):
        ufrac = utime / float(total)
        sfrac = stime / float(total)
        return (ufrac, sfrac)

    def _on_starting(self):
        super(CpuStatsCollector, self)._on_starting()
        self._log.info('Collecting CPU stats every {0} seconds.'.format(self._interval))

    def _on_stopped(self):
        if self._last_pid is not None and self._first_total_time != self._last_total_time:
            u, s = self.calc_fracs(self._last_total_time - self._first_total_time,
                                   self._sum_utime + self._last_utime,
                                   self._sum_stime + self._last_stime)
            self._log.info('Total CPU avg: {0:0.2f} user, {1:0.2f} system.'.format(u, s))
        super(CpuStatsCollector, self)._on_stopped()
