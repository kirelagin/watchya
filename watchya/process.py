import errno
import logging
log = logging.getLogger(__name__)
import os

from twisted.internet import protocol
from twisted.internet.error import ProcessExitedAlready

from watchya import notify


class ManagedProcessProtocol(protocol.ProcessProtocol):
    def __init__(self, manager):
        self._manager = manager

    def connectionMade(self):
        self._manager._process_created(self.transport)

    def processEnded(self, status):
        self._manager._process_ended(status)

    def outReceived(self, data):
        for line in data.strip().split('\n'):
            log.info('(managed stdout) ' + line)

    def errReceived(self, data):
        for line in data.strip().split('\n'):
            log.info('(managed stderr) ' + line)


class ForkedProcess(object):
    """This class provides signalProcess method.

    It's like twisted.internet.process.Process but has only one
    of its methods. I feel, I should've introduced a new interface,
    but, well, that's Python.

    """

    def __init__(self, pid):
        self._pid = pid

    @property
    def pid(self):
        return self._pid

    def signalProcess(self, signal):
        try:
            return os.kill(self._pid, signal)
        except OSError as e:
            if e.errno == errno.ESRCH:
                raise ProcessExitedAlready
            else:
                raise
