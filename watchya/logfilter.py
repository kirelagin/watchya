import logging
log = logging.getLogger(__name__)
import os
import re
import stat

from twisted.internet import abstract, main, fdesc
from twisted.internet.protocol import Protocol


class NamedPipe(abstract.FileDescriptor):
    def __init__(self, name, protocol, reactor=None):
        abstract.FileDescriptor.__init__(self, reactor)
        self._name = name
        self.protocol = protocol
        self.protocol.makeConnection(self)

    def startReading(self):
        name = self._name
        if os.path.exists(name) and not stat.S_ISFIFO(os.stat(name).st_mode):
            os.remove(name)
        if not os.path.exists(name):
            os.mkfifo(name)
        self._fp = os.open(name, os.O_NONBLOCK | os.O_RDONLY)
        abstract.FileDescriptor.startReading(self)

    def doRead(self):
        data = os.read(self._fp, 4096)
        if data:
            self.protocol.dataReceived(data)
        else:
            self.connectionLost(main.CONNECTION_DONE)
            self.protocol.connectionLost()

    def fileno(self):
        return self._fp

class LogFilter(Protocol):
    def __init__(self, pattern, flags=0):
        self._pattern = re.compile(pattern, flags) if pattern is not None else None
        self._log_unmatched = log
        self._log_matched = logging.getLogger(__name__ + '.matched')

    def makeConnection(self, pipe):
        log.info('Watching logfile "{0}".'.format(pipe._name))

    def dataReceived(self, data):
        lines = data.split('\n')
        for line in lines:
            if not line:
                continue
            if self._pattern is None or self._pattern.match(line) is not None:
                self._log_matched.warning(line)
            else:
                self._log_unmatched.debug(line)
