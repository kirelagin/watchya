#!/usr/bin/env python2.6

import errno
import logging
log = logging.getLogger('watchya')
from optparse import OptionParser
import os.path
import signal
import sys

from twisted.internet import epollreactor
epollreactor.install()
from twisted.internet import reactor
from twisted.python import log as twisted_log

from watchya.log import FilterBelow
from watchya.unitfile import Unit
from watchya.manager import Manager


if __name__ == '__main__':
    # Parsing arguments (argparse is available since Python 2.7 =( )
    parser = OptionParser()
    parser.set_defaults(stdout_loglevel=logging.INFO)
    parser.add_option('-d', '--directory',
                      action='append', dest='unit_dirs', type='string',
                      help='Look for service unit file in this directory')
    parser.add_option('-v', '--debug',
                      action='store_const',
                      const=logging.DEBUG, dest='stdout_loglevel',
                      help='Look for service unit file in this directory')
    (options, args) = parser.parse_args()
    if len(args) != 1:
        raise Exception('Unit file name missing')
    unit_name = args[0]
    if not options.unit_dirs:
        raise Exception('You must specify at least one directory ' +
                        'to search for unit files (-d option)')

    # Setting up logging
    # Send WARNING and above to stderr, everything else -- to stdout,
    # so you can easily send all DEBUG and INFO to /dev/null.
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s [%(name)s] %(message)s')

    warn_handler = logging.StreamHandler(sys.stderr)
    warn_handler.setLevel(logging.WARNING)
    warn_handler.setFormatter(formatter)

    others_handler = logging.StreamHandler(sys.stdout)
    others_handler.setLevel(options.stdout_loglevel)
    others_handler.addFilter(FilterBelow(logging.WARN))
    others_handler.setFormatter(formatter)

    root.addHandler(warn_handler)
    root.addHandler(others_handler)

    twisted_log.PythonLoggingObserver().start()


    # Loading unit file
    unit = None
    for unit_dir in options.unit_dirs:
        try:
            unit = Unit(unit_dir, unit_name)
            log.info('Using unit from "{0}".'.format(unit.path))
        except IOError as e:
            if e.errno == errno.ENOENT: # file does not exist
                continue
            else:
                raise e

    if not unit:
        raise IOError(errno.ENOENT, 'Unit file was not found')

    def sig_handler(*args):
        def stop_then_crash(reactor):
            t = reactor.stop
            reactor.stop = reactor.crash
            t()
        reactor.callFromThread(stop_then_crash, reactor)
    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    manager = Manager(unit)
    manager.run(reactor, installSignalHandlers=False)

    logging.shutdown()
