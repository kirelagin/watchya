#!/usr/bin/env python2.6


from __future__ import print_function
import random
import sys
import time

from watchya.sd_daemon import sd_notifym


if __name__ == '__main__':
    def log(m):
        print(m, file=sys.stderr)

    log('started')
    log('args: {0}'.format(sys.argv))
    while 1:
        if random.randint(0, 9) == 4:
            log('failing')
            sys.exit(random.randint(1, 3))
        time.sleep(1)
