#!/usr/bin/env python2.6


from __future__ import print_function
import datetime
import os
import random
import signal
import sys
import time

from watchya.sd_daemon import sd_notifym
from watchya.utils import microseconds


if __name__ == '__main__':
    def log(m):
        print(m, file=sys.stderr)

    log('starting')
    time.sleep(2)
    log('started')
    sd_notifym({'READY': 1})
    last_ping = None
    ping_usecs = os.environ.get('WATCHDOG_USEC', None)
    status = []
    if ping_usecs:
        try:
            ping_usecs = int(ping_usecs)/2
            log('pinging enabled')
            status.append('pinging every {0}us'.format(ping_usecs))
        except:
            ping_usecs = None
    if not ping_usecs:
        log('pinging disabled')
        status.append('not pinging')

    signal.signal(signal.SIGINT, signal.SIG_IGN)
    if random.randint(1, 3) == 1:
        signal.signal(signal.SIGTERM, signal.SIG_IGN)
        status.append('ignoring SIGTERM :p')

    sd_notifym({'STATUS': ', '.join(status)})

    while 1:
        n = datetime.datetime.now()
        if ping_usecs and (not last_ping or microseconds(n - last_ping) > ping_usecs):
            if sd_notifym({'WATCHDOG': 1}) > 0:
                last_ping = n

        if random.randint(0, 9) == 4:
            log('hanging')
            while 1:
                time.sleep(1)
        time.sleep(1)
